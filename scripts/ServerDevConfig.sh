#!/bin/bash

REPO_PATH="git"
APP_DEV_FILE="application-dev.yml"
APP_TEST_FILE="application-test.yml"
APP_AWS_FILE="application-aws.yml"
APP_STAGE_FILE="application-stage.yml"
APP_DB_FILE="application-db.yml"
TARGET_DIR="divelogbook/server/src/main/resources"
SOURCE_DIR="divelogbook.private"



if test -d ~/$REPO_PATH/$SOURCE_DIR;
then
    echo "~/$REPO_PATH/$SOURCE_DIR exists. we have nothing to do - maybe pull the repo?" 
else
    echo "~/$REPO_PATH/$SOURCE_DIR does not exist. So we clone the repo"
    # Clone git repo divelogbook.private
    git clone git@gitlab.com:$1/divelogbook.private.git ~/$REPO_PATH/$SOURCE_DIR
fi

#echo "create the dev aswell the test application file"
ln -sf ~/$REPO_PATH/$SOURCE_DIR/$APP_DEV_FILE ~/$REPO_PATH/$TARGET_DIR/$APP_DEV_FILE
ln -sf ~/$REPO_PATH/$SOURCE_DIR/$APP_TEST_FILE ~/$REPO_PATH/$TARGET_DIR/$APP_TEST_FILE
ln -sf ~/$REPO_PATH/$SOURCE_DIR/$APP_STAGE_FILE ~/$REPO_PATH/$TARGET_DIR/$APP_STAGE_FILE

mkdir ~/$REPO_PATH/$TARGET_DIR/configurations/
ln -sf ~/$REPO_PATH/$SOURCE_DIR/$APP_AWS_FILE ~/$REPO_PATH/$TARGET_DIR/configurations/$APP_AWS_FILE
ln -sf ~/$REPO_PATH/$SOURCE_DIR/$APP_DB_FILE ~/$REPO_PATH/$TARGET_DIR/configurations/$APP_DB_FILE
