package ch.hotmilk.dlb.server.controller;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.io.InputStream;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import ch.hotmilk.dlb.server.service.DiveFileUploadService;

@ExtendWith(MockitoExtension.class)
class DiveFileControllerTest {

	@Mock
	private DiveFileUploadService uploadServiceMock;

	@InjectMocks
	private DiveFileController fileControllerUnderTest;

	private static MultipartFile file;

	@BeforeAll
	private static void prepareClass() {
		file = new MockMultipartFile("file", "hello.txt", MediaType.TEXT_PLAIN_VALUE, "Hello, World!".getBytes());
	}

	@Test
	void uploadFile_getOK_Status() throws IOException {

		HttpStatus uploadFile = fileControllerUnderTest.uploadFile(file);

		verify(uploadServiceMock, times(1)).saveDiveFile(Mockito.any(InputStream.class));
		Assertions.assertEquals(HttpStatus.OK, uploadFile);
	}

	@Test
	void uploadFile_getINTERNAL_SERVER_ERROR_Status() throws IOException {
		when(uploadServiceMock.saveDiveFile(Mockito.any(InputStream.class))).thenThrow(new IOException());

		HttpStatus uploadFile = fileControllerUnderTest.uploadFile(file);

		verify(uploadServiceMock, times(1)).saveDiveFile(Mockito.any(InputStream.class));
		Assertions.assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, uploadFile);
	}
}
