package ch.hotmilk.dlb.server.service;

import static org.assertj.core.api.Assertions.assertThatObject;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import ch.hotmilk.dlb.server.model.Dive;
import ch.hotmilk.dlb.server.model.Userpool;
import ch.hotmilk.dlb.server.repository.DiveRepository;
import ch.hotmilk.dlb.server.service.DiveService;

@ExtendWith(MockitoExtension.class)
class DiveServiceTest {

	@Mock
	private DiveRepository diveRepo;

	@InjectMocks
	private DiveService diveServiceUnderTest;

	private List<Dive> getDives() {
		ArrayList<Dive> dives = new ArrayList<Dive>();
		dives.add(new Dive());
		dives.add(new Dive());
		return dives;
	}

	@Test
	void getAllDives() {
		Mockito.when(diveRepo.findAll()).thenReturn(getDives());

		List<Dive> allDives = diveServiceUnderTest.getAllDives();
		assertEquals(2, allDives.size());
		verify(diveRepo, times(1)).findAll();
	}

	@Test
	void getDivesFromUser() {
		Userpool userpool = new Userpool();
		Mockito.when(diveRepo.findAllFromUser(userpool.getSub())).thenReturn(getDives());

		List<Dive> allDivesFromUser = diveServiceUnderTest.getAllDivesFromUser(new Userpool());
		assertEquals(2, allDivesFromUser.size());
	}

	@Test
	void getDiveFromId() {
		Mockito.when(diveRepo.findByIdFromUser(1, new Userpool().getSub())).thenReturn(Optional.of(new Dive()));

		Dive dive = diveServiceUnderTest.getDive(1, new Userpool());
		assertThatObject(dive);
	}

	@Test
	void getNullFromId() {
		Mockito.when(diveRepo.findByIdFromUser(1, new Userpool().getSub())).thenReturn(Optional.ofNullable(null));

		Dive dive = diveServiceUnderTest.getDive(1, new Userpool());
		assertThatObject(dive);
	}

}
