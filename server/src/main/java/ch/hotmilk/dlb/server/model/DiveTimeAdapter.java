package ch.hotmilk.dlb.server.model;

import java.io.IOException;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

public class DiveTimeAdapter extends StdDeserializer<LocalTime> {

	private static final long serialVersionUID = -8577850943821543660L;

	public DiveTimeAdapter() {
		this(null);
	}

	protected DiveTimeAdapter(Class<?> vc) {
		super(vc);
	}

	@Override
	public LocalTime deserialize(JsonParser p, DeserializationContext ctxt)
			throws IOException, JsonProcessingException {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm:ss");
		return LocalTime.parse(p.getText(), formatter);
	}
}
