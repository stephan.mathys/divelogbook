package ch.hotmilk.dlb.server.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ch.hotmilk.dlb.server.model.Dive;
import ch.hotmilk.dlb.server.model.Userpool;
import ch.hotmilk.dlb.server.repository.DiveRepository;
import ch.hotmilk.dlb.server.repository.GoogleMapsCoordinatesRepository;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j

public class DiveService {

	@Autowired
	private DiveRepository diveRepo;

	@Autowired
	private GoogleMapsCoordinatesRepository googleMapsRepo;

	public List<Dive> getAllDives() {
		Iterable<Dive> findAllDives = diveRepo.findAll();
		ArrayList<Dive> listDives = new ArrayList<Dive>();
		findAllDives.forEach(listDives::add);
		log.debug("found " + listDives.size() + " dives in the database");

		return listDives;
	}

	public List<Dive> getAllDivesFromUser(Userpool user) {
		Iterable<Dive> findAllDives = diveRepo.findAllFromUser(user.getSub());
		ArrayList<Dive> listDives = new ArrayList<Dive>();
		findAllDives.forEach(listDives::add);
		return listDives;
	}

	/**
	 * 
	 * @param diveId Id from the dive in the database
	 * @param user   has the right to the dive
	 * @return the dive or null if no dive with the id or the user can be found
	 */
	public Dive getDive(Integer diveId, Userpool user) {
		Optional<Dive> optionalDive = diveRepo.findByIdFromUser(diveId, user.getSub());
		return optionalDive.isEmpty() ? null : optionalDive.get();
	}

	public void updateCoordinates(List<Dive> updateDives) {

		for (Dive dive : updateDives) {
			Optional<Dive> diveToSave = diveRepo.findById(dive.getId());
			diveToSave.get().setGoogleMapsCoordinates(dive.getGoogleMapsCoordinates());
			Dive savedDive = diveRepo.save(diveToSave.get());
			log.debug(savedDive.toString());
		}

	}
}
