package ch.hotmilk.dlb.server.security;

import static java.util.List.of;

import java.util.Enumeration;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;

import com.nimbusds.jose.proc.SecurityContext;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.proc.ConfigurableJWTProcessor;

import ch.hotmilk.dlb.server.model.Userpool;
import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class AwsCognitoIdTokenProcessor {

	@Autowired
	private JwtConfiguration jwtConfiguration;

	@Autowired
	private ConfigurableJWTProcessor<SecurityContext> configurableJWTProcessor;

	public Authentication authenticate(HttpServletRequest request) throws Exception {
		String idToken = request.getHeader(this.jwtConfiguration.getHttpHeader());
		Enumeration<String> headerNames = request.getHeaderNames();

		while (headerNames.hasMoreElements()) {
			String headerName = headerNames.nextElement();
			log.debug(headerName + ": " + request.getHeader(headerName));
		}

		if (idToken != null) {
			JWTClaimsSet claims = this.configurableJWTProcessor.process(this.getBearerToken(idToken), null);
			validateIssuer(claims);
			verifyIfIdToken(claims);
			String username = getUserNameFrom(claims);
			if (username != null) {
				List<GrantedAuthority> grantedAuthorities = of(new SimpleGrantedAuthority("ROLE_ADMIN"));
//				User user = new User(username, "", of());
				return new JwtAuthentication(getUserFromClaim(claims), claims, grantedAuthorities);
			}
		}
		return null;
	}

	private String getUserNameFrom(JWTClaimsSet claims) {
		Map<String, Object> claims2 = claims.getClaims();
		claims2.forEach((k, v) -> log.debug("key: " + k + " - value: " + v));
		return claims.getClaims().get(this.jwtConfiguration.getUserNameField()).toString();
	}

	private Userpool getUserFromClaim(JWTClaimsSet claims) {
		Map<String, Object> claims2 = claims.getClaims();
		Userpool userpool = new Userpool();
		userpool.setSub(claims2.get(jwtConfiguration.getUserSubField()).toString());
		claims2.forEach((k, v) -> log.debug("key: " + k + " - value: " + v));
		return userpool;
	}

	private void verifyIfIdToken(JWTClaimsSet claims) throws Exception {
		if (!claims.getIssuer().equals(this.jwtConfiguration.getCognitoIdentityPoolUrl())) {
			throw new Exception("JWT Token is not an ID Token");
		}
	}

	private void validateIssuer(JWTClaimsSet claims) throws Exception {
		if (!claims.getIssuer().equals(this.jwtConfiguration.getCognitoIdentityPoolUrl())) {
			throw new Exception(String.format("Issuer %s does not match cognito idp %s", claims.getIssuer(),
					this.jwtConfiguration.getCognitoIdentityPoolUrl()));
		}
	}

	private String getBearerToken(String token) {
		return token.startsWith("Bearer ") ? token.substring("Bearer ".length()) : token;
	}
}
