package ch.hotmilk.dlb.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import lombok.extern.slf4j.Slf4j;

@SpringBootApplication
@Slf4j
public class DiveApplication {

	public static void main(String[] args) {
		SpringApplication.run(DiveApplication.class, args);
	}
}
