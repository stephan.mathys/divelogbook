package ch.hotmilk.dlb.server.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
@JsonIgnoreProperties(value = { "dive" })
@XmlAccessorType(XmlAccessType.FIELD)
@Entity
public class Gasmix {

	@Getter
	@Setter
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Getter
	@Setter
	@JsonAlias({ "num" })
	private Integer number;

	@Getter
	@Setter
	@JsonAlias({ "o2" })
	private Double oxygen;

	@Getter
	@Setter
	@JsonAlias({ "n2" })
	private Double nitrogen;

	@Getter
	@Setter
	@JsonAlias({ "he" })
	private Double helium;

	@Getter
	@Setter
	@ManyToOne
	@JoinColumn(name = "dive_id")
	@ToString.Exclude
	private Dive dive;

	public String getDescription() {

		if (oxygen == 21) {
			return "Air";
		} else if (oxygen > 21 && helium.equals(0.0)) {
			return "Nitrox";
		}
		return "Helium"; // Don't now what the Helium thing is...
	}

	public Boolean isNitrox() {
		if (oxygen > 21 && helium.equals(0.0)) {
			return true;
		}
		return false;
	}

}
