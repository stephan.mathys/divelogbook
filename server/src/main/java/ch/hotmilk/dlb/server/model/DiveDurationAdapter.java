package ch.hotmilk.dlb.server.model;

import java.io.IOException;
import java.time.Duration;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

public class DiveDurationAdapter extends StdDeserializer<Duration> {

	private static final long serialVersionUID = 5389948716523040536L;

	protected DiveDurationAdapter(Class<?> vc) {
		super(vc);
	}

	public DiveDurationAdapter() {
		this(null);
	}

	@Override
	public Duration deserialize(JsonParser p, DeserializationContext ctxt) throws IOException, JsonProcessingException {
		return Duration.parse("PT" + p.getText() + "S");
	}

}
