package ch.hotmilk.dlb.server.service;

import java.io.IOException;
import java.io.InputStream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;

import ch.hotmilk.dlb.server.model.Dive;
import ch.hotmilk.dlb.server.model.DiveComputer;
import ch.hotmilk.dlb.server.model.DiveSample;
import ch.hotmilk.dlb.server.model.Divelog;
import ch.hotmilk.dlb.server.model.Userpool;
import ch.hotmilk.dlb.server.repository.DecoRepository;
import ch.hotmilk.dlb.server.repository.DiveComputerRepository;
import ch.hotmilk.dlb.server.repository.DiveRepository;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class DiveFileUploadService {

	@Autowired
	DiveComputerRepository diveComputerRepo;

	@Autowired
	DiveRepository diveRepo;

	@Autowired
	DecoRepository decoRepo;

	public String saveDiveFile(InputStream file) throws IOException {
		return saveDiveFile(file, null);
	}

	public String saveDiveFile(InputStream file, Userpool userpool) throws IOException {

		XmlMapper xmlMapper = new XmlMapper();

		Divelog divelog = xmlMapper.readValue(file, Divelog.class);
		DiveComputer dc = new DiveComputer(divelog.getVendor(), divelog.getProduct(), divelog.getModel());
		DiveComputer savedDc = diveComputerRepo.findDiveComputerByVendorAndProductAndModel(dc.getVendor(),
				dc.getProduct(), dc.getModel());
		if (savedDc == null) {
			dc = (diveComputerRepo.save(dc));
		} else {
			dc = savedDc;
		}
		for (Dive dive : divelog.getDives()) {
			dive.setComputer(dc);
			if (userpool != null) {
				dive.setUserpoolSub(userpool.getSub());
			}
			dive.getGasmixes().forEach(gasmix -> gasmix.setDive(dive));
			for (DiveSample sample : dive.getSamples()) {
				sample.setDive(dive);
				sample.setDeco(decoRepo.save(sample.getDeco()));
				sample.getDeco().setEvent(sample);
			}
		}
		diveRepo.saveAll(divelog.getDives());
		divelog.getDives().forEach(dive -> log.debug(dive.toString()));
		return "ok";
	}
}