package ch.hotmilk.dlb.server.json.serializer;

import java.io.IOException;
import java.time.format.DateTimeFormatter;
import java.util.Optional;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

import ch.hotmilk.dlb.server.model.Dive;
import ch.hotmilk.dlb.server.model.GoogleMapsCoordinates;

public class DiveTimeAndDateSerializer extends StdSerializer<Dive> {

	public DiveTimeAndDateSerializer() {
		this(null);
	}

	protected DiveTimeAndDateSerializer(Class<Dive> t) {
		super(t);
	}

	private static final long serialVersionUID = -211567474466119449L;

	@Override
	public void serialize(Dive dive, JsonGenerator gen, SerializerProvider provider) throws IOException {
		DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("dd-MM-yyyy");
		DateTimeFormatter timeFormat = DateTimeFormatter.ofPattern("hh:mm:ss");

		gen.writeStartObject();
		gen.writeNumberField("id", dive.getId());
		gen.writeStringField("date", dateFormat.format(dive.getDiveDate()));
		gen.writeStringField("time", timeFormat.format(dive.getDiveStartTime()));
		gen.writeBooleanField("changed", false);

		Optional<GoogleMapsCoordinates> googleMapsCoordinates = Optional.ofNullable(dive.getGoogleMapsCoordinates());
		gen.writeObjectField("googleMapsCoordinates",
				googleMapsCoordinates.orElseGet(() -> new GoogleMapsCoordinates()));

		gen.writeEndObject();
	}

}
