package ch.hotmilk.dlb.server.model;

import java.io.IOException;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.type.MapType;

public class DiveEventDeserializer extends StdDeserializer<DiveSample> {

	public DiveEventDeserializer() {
		this(null);
	}

	protected DiveEventDeserializer(Class<?> vc) {
		super(vc);
	}

	private static final long serialVersionUID = -5558092097391464095L;

	private List<Map<String, Object>> readParticipantsMap(JsonParser p, DeserializationContext ctxt)
			throws IOException {
		MapType mapType = ctxt.getTypeFactory().constructMapType(Map.class, String.class, Object.class);
		JsonDeserializer<Object> mapDeserializer = ctxt.findRootValueDeserializer(mapType);
		List<Map<String, Object>> events = new ArrayList<>();
		p.nextToken(); // skip Start of Participants object
		while (p.currentToken() == JsonToken.FIELD_NAME) {
			p.nextToken(); // skip start of Participant
			Object event = mapDeserializer.deserialize(p, ctxt);
			events.add((Map<String, Object>) event);
			p.nextToken(); // skip end of Participant
		}

		return events;
	}

	@Override
	public DiveSample deserialize(JsonParser parser, DeserializationContext ctxt)
			throws IOException, JsonProcessingException {
		DiveSample sample = new DiveSample();

		MapType mapType = ctxt.getTypeFactory().constructMapType(Map.class, String.class, Object.class);

		JsonDeserializer<Object> mapDeserializer = ctxt.findRootValueDeserializer(mapType);
		Map<String, Object> attributeMap = (Map<String, Object>) mapDeserializer.deserialize(parser, ctxt);

		attributeMap.forEach((k, v) -> {
			System.out.println("key: " + k + " - " + "value: " + v);

			switch (k) {
			case "time":
				sample.setTime(parseDurationFromString(((String) v)));
				break;
			case "depth":
				sample.setDepth(getDouble(v, "value"));
				break;
			case "temp":
				sample.setWatherTemperature(getDouble(v, "value"));
				break;
			case "gaschange":
				sample.setGasChange(getInt(v));
				break;
			case "deco":
				sample.setDeco(constructDeco(v));
				break;
			default:
				break;
			}
		});

		return sample;
	}

	private Duration parseDurationFromString(String str) {
		return Duration.parse("PT" + str + "S");
	}

	private Deco constructDeco(Object v) {
		Map m = (Map) v;

		Double depth = getDouble(m, "depth");
		String type = (String) m.get("type");
		Duration duration = parseDurationFromString((String) m.get("duration"));

		return new Deco(depth, type, duration);
	}

	private Integer getInt(Object v) {
		Map m = (Map) v;
		return Integer.parseInt((String) m.get("mix"));
	}

	private Double getDouble(Object v, String attr) {
		Map m = (Map) v;
		return Double.parseDouble((String) m.get(attr));
	}
}
