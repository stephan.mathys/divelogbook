package ch.hotmilk.dlb.server.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import ch.hotmilk.dlb.server.model.DiveComputer;

public interface DiveComputerRepository extends CrudRepository<DiveComputer, Integer> {

	public static final String FIND_DIVE_COMPUTER = "SELECT dc FROM DiveComputer dc WHERE dc.vendor = :vendor AND dc.product = :product AND dc.model = :model";

	@Query(FIND_DIVE_COMPUTER)
	DiveComputer findDiveComputerByVendorAndProductAndModel(@Param("vendor") String vendor,
			@Param("product") String product, @Param("model") String model);

}
