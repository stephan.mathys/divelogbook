package ch.hotmilk.dlb.server;

import static com.nimbusds.jose.JWSAlgorithm.RS256;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Transactional;

import com.nimbusds.jose.jwk.source.JWKSource;
import com.nimbusds.jose.jwk.source.RemoteJWKSet;
import com.nimbusds.jose.proc.JWSKeySelector;
import com.nimbusds.jose.proc.JWSVerificationKeySelector;
import com.nimbusds.jose.proc.SecurityContext;
import com.nimbusds.jose.util.DefaultResourceRetriever;
import com.nimbusds.jose.util.ResourceRetriever;
import com.nimbusds.jwt.proc.ConfigurableJWTProcessor;
import com.nimbusds.jwt.proc.DefaultJWTProcessor;

import ch.hotmilk.dlb.server.model.Dive;
import ch.hotmilk.dlb.server.model.GoogleMapsCoordinates;
import ch.hotmilk.dlb.server.model.Keyword;
import ch.hotmilk.dlb.server.model.Userpool;
import ch.hotmilk.dlb.server.repository.DiveRepository;
import ch.hotmilk.dlb.server.repository.KeywordRepo;
import ch.hotmilk.dlb.server.repository.UserRepository;
import ch.hotmilk.dlb.server.security.JwtConfiguration;
import ch.hotmilk.dlb.server.service.DiveFileUploadService;
import lombok.extern.slf4j.Slf4j;

@EnableTransactionManagement
@Configuration
@Slf4j
public class Configurations {

	@Autowired
	ResourceLoader resloader;

	@Autowired
	DiveRepository diveRepo;

	@Autowired
	KeywordRepo keywordRepo;

	@Autowired
	UserRepository userRepo;

	@Autowired
	DiveFileUploadService uploadService;

	@Autowired
	private JwtConfiguration jwtConfiguration;

//	@Bean
	@Transactional
	@ConditionalOnProperty(prefix = "ch.hotmilk", name = "load-sample-dives", havingValue = "true", matchIfMissing = false)
	public CommandLineRunner loadSampleDives(Environment env) {
		return (args) -> {
			log.debug("load sample dives to database");
			Resource resource = resloader.getResource("classpath:sampleDive.xml");
			if (resource.exists()) {
				Userpool userpool = new Userpool();
				userpool.setSub(env.getProperty("ch.hotmilk.user-pool-sub"));
				userRepo.save(userpool);
				uploadService.saveDiveFile(resource.getInputStream(), userpool);
				Iterable<Keyword> savedKeywords = keywordRepo.saveAll(prepareKeywords());
				Iterable<Dive> findAll = diveRepo.findAll();
				Iterator<Dive> it = findAll.iterator();
				if (it.hasNext()) {
					Dive dive = it.next();
//						log.debug("found a dive: " + dive.toString());
//						List<Keyword> lstKeyword = new ArrayList<Keyword>();
					savedKeywords.forEach(k -> dive.addKeyword(k));

					GoogleMapsCoordinates coord = new GoogleMapsCoordinates();
					coord.setLatitude(40.71371526322932);
					coord.setLongitude(19.355081543385865);
					dive.setGoogleMapsCoordinates(coord);
//						dive.getKeywords().add(new Keyword("myKeyword"));
					diveRepo.save(dive);
				}

			}
		};
	}

	@Bean
	public ConfigurableJWTProcessor<SecurityContext> configurableJWTProcessor() throws MalformedURLException {
		ResourceRetriever resourceRetriever = new DefaultResourceRetriever(jwtConfiguration.getConnectionTimeout(),
				jwtConfiguration.getReadTimeout());
		URL jwkSetURL = new URL(jwtConfiguration.getJwkUrl());
		JWKSource<SecurityContext> keySource = new RemoteJWKSet<SecurityContext>(jwkSetURL, resourceRetriever);
		ConfigurableJWTProcessor<SecurityContext> jwtProcessor = new DefaultJWTProcessor<SecurityContext>();
		JWSKeySelector<SecurityContext> keySelector = new JWSVerificationKeySelector<SecurityContext>(RS256, keySource);
		jwtProcessor.setJWSKeySelector(keySelector);
		return jwtProcessor;
	}

	private List<Keyword> prepareKeywords() {
		List<Keyword> keywordList = new ArrayList<Keyword>();
		keywordList.add(new Keyword("Dolfin"));
		keywordList.add(new Keyword("Turtle"));
		keywordList.add(new Keyword("Shark"));
		keywordList.add(new Keyword("Wale"));
		keywordList.add(new Keyword("Waleshark"));
		keywordList.add(new Keyword("Manta"));

		return keywordList;
	}
}
