package ch.hotmilk.dlb.server.model;

import java.time.Duration;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
@JsonIgnoreProperties(value = { "dive" })
@JsonDeserialize(using = DiveEventDeserializer.class)
@Entity
public class DiveSample {

	@Getter
	@Setter
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Getter
	@Setter
	@JsonDeserialize(using = DiveDurationAdapter.class)
	private Duration time;

	@Getter
	@Setter
	private Double depth;

	@Getter
	@Setter
	private Double watherTemperature;

	@Getter
	@Setter
	@ManyToOne
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "deco_id", referencedColumnName = "id")
	@ToString.Exclude
	private Deco deco;

	@Getter
	@Setter
	private Integer gasChange;

	@Getter
	@Setter
	@ManyToOne
	@JoinColumn(name = "dive_id")
	@ToString.Exclude
	private Dive dive;

//	@Getter
//	@Setter
//	private List<SampleEvent> events;

}
