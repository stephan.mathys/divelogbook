package ch.hotmilk.dlb.server.repository;

import org.springframework.data.repository.CrudRepository;

import ch.hotmilk.dlb.server.model.Userpool;

public interface UserRepository extends CrudRepository<Userpool, Integer> {

}
