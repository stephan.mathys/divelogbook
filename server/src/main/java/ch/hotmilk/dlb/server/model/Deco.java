package ch.hotmilk.dlb.server.model;

import java.time.Duration;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@ToString
@Entity
@NoArgsConstructor
@JsonIgnoreProperties("event")
public class Deco {

	public Deco(Double depth, String type, Duration duration) {
		this.depth = depth;
		this.type = type;
		this.duration = duration;
	}

	@Getter
	@Setter
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Getter
	@Setter
	private Double depth;

	@Getter
	@Setter
	private String type;

	@Getter
	@Setter
	private Duration duration;

	@Getter
	@Setter
	@OneToOne(mappedBy = "deco")
	private DiveSample event;
}
