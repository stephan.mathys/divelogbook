package ch.hotmilk.dlb.server.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
public class Divelog {

	@Getter
	@Setter
	private String vendor;

	@Getter
	@Setter
	private List<Dive> dives;

	@Getter
	@Setter
	private String product;

	@Getter
	@Setter
	private String model;
}
