package ch.hotmilk.dlb.server.repository;

import org.springframework.data.repository.CrudRepository;

import ch.hotmilk.dlb.server.model.DiveSample;

public interface DiveSampleRepository extends CrudRepository<DiveSample, Integer> {

}
