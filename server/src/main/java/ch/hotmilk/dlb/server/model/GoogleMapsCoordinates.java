package ch.hotmilk.dlb.server.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@ToString
@Entity
@NoArgsConstructor
public class GoogleMapsCoordinates {

	@Getter
	@Setter
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Getter
	@Setter
	@JsonProperty("lat")
	private Double latitude;

	@Getter
	@Setter
	@JsonProperty("lng")
	private Double longitude;

	@OneToOne(mappedBy = "googleMapsCoordinates")
	private Dive dive;

}
