package ch.hotmilk.dlb.server.repository;

import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import ch.hotmilk.dlb.server.model.Dive;
import ch.hotmilk.dlb.server.model.GoogleMapsCoordinates;

public interface DiveRepository extends CrudRepository<Dive, Integer> {

	@Query(value = "SELECT d FROM Dive d where d.userpoolSub = :userpoolSub")
	public Iterable<Dive> findAllFromUser(@Param("userpoolSub") String userpoolSub);

	@Query(value = "SELECT d FROM Dive d where d.id = :diveId and d.userpoolSub = :userpoolSub")
	public Optional<Dive> findByIdFromUser(@Param("diveId") Integer diveId, @Param("userpoolSub") String userpoolSub);

	@Transactional
	@Modifying
	@Query(value = "UPDATE Dive d set googleMapsCoordinates = :googleMapsCoordinates where d.id = :diveId")
	public void saveCoordinates(@Param("diveId") Integer diveId,
			@Param("googleMapsCoordinates") GoogleMapsCoordinates googleMapsCoordinates);
}
