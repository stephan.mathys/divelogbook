package ch.hotmilk.dlb.server.model;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.regex.Pattern;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

public class DiveDateAdapter extends StdDeserializer<LocalDate> {

	private static final long serialVersionUID = 1L;

	private static Pattern DATE_PATTERN_AMERICAN = Pattern.compile("^\\d{4}-\\d{2}-\\d{2}$");

	public DiveDateAdapter() {
		this(null);
	}

	protected DiveDateAdapter(Class<?> vc) {
		super(vc);
	}

	@Override
	public LocalDate deserialize(JsonParser p, DeserializationContext ctxt)
			throws IOException, JsonProcessingException {
		String text = p.getText();
		DateTimeFormatter formatter;
		if (text.matches(DATE_PATTERN_AMERICAN.pattern())) {
			formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		} else {
			formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
		}

		return LocalDate.parse(text, formatter);
	}

}
