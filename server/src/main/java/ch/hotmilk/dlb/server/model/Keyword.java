package ch.hotmilk.dlb.server.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@ToString
@Entity
@NoArgsConstructor
@JsonIgnoreProperties({ "dives" })
public class Keyword {

	@Getter
	@Setter
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	public Keyword(String name) {
		this(name, null);
	}

	public Keyword(String name, Dive dive) {
		this.name = name;
		this.dives.add(dive);
	}

	@Getter
	@Setter
	private String name;

	@Getter
	@Setter
	@ManyToMany(mappedBy = "keywords")
	@ToString.Exclude
	private List<Dive> dives = new ArrayList<Dive>();
}
