package ch.hotmilk.dlb.server.model;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import ch.hotmilk.dlb.server.model.adapter.DiveMaxDepthAdapter;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
@Entity
public class Dive {

	@JsonInclude()
	@Transient
	@Getter
	@Setter
	private Boolean changed;

	@Getter
	@Setter
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Getter
	@Setter
	@JsonProperty("number")
	private Integer internalNumber;

	@Getter
	@Setter
	@JsonProperty("date")
	@JsonDeserialize(using = DiveDateAdapter.class)
	private LocalDate diveDate;

	@Getter
	@Setter
	@JsonAlias({ "time" })
	@JsonDeserialize(using = DiveTimeAdapter.class)
	private LocalTime diveStartTime;

	@Getter
	@Setter
	@JsonProperty("mode")
	@Enumerated(EnumType.STRING)
	private DiveMode diveMode;

	@Getter
	@Setter
	@JsonProperty("duration")
	@JsonDeserialize(using = DiveDurationAdapter.class)
	private Duration diveDuration;

	@Getter
	@Setter
	private String fingerprint;

	@Getter
	@Setter
	@OneToMany(mappedBy = "dive", cascade = CascadeType.PERSIST)
	private List<Gasmix> gasmixes;

	@Getter
	@Setter
	@JsonProperty("depth")
	@JsonDeserialize(using = DiveMaxDepthAdapter.class)
	private Double maxDepth;

	@Getter
	@Setter
	@OneToMany(mappedBy = "dive", cascade = CascadeType.PERSIST)
	private List<DiveSample> samples;

	@Getter
	@Setter
	@ManyToOne
	@JoinColumn(name = "dive_computer_id")
	private DiveComputer computer;

	@Getter
	@Setter
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "google_maps_coordinates_id")
	private GoogleMapsCoordinates googleMapsCoordinates;

	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "dive_keyword", joinColumns = @JoinColumn(name = "dive_id"), inverseJoinColumns = @JoinColumn(name = "keyword_id"))
	@JsonProperty("keywords")
	private List<Keyword> keywords = new ArrayList<Keyword>();

	@Getter
	@Setter
	private String userpoolSub;

	public LocalTime getDiveEndTime() {
		return diveStartTime.plus(diveDuration);
	}

	public Boolean hasChanged() {
		return changed;
	}

	public void addKeyword(Keyword k) {
		k.getDives().add(this);
		this.keywords.add(k);
	}
}
