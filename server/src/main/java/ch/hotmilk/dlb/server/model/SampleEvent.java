package ch.hotmilk.dlb.server.model;

import java.time.Duration;

import lombok.Getter;
import lombok.Setter;

public class SampleEvent {

	@Getter
	@Setter
	private SampleEventType type;

	@Getter
	@Setter
	private Duration duration;

	@Getter
	@Setter
	private Integer flag;
}
