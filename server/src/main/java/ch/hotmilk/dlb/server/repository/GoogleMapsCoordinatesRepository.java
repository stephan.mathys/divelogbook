package ch.hotmilk.dlb.server.repository;

import org.springframework.data.repository.CrudRepository;

import ch.hotmilk.dlb.server.model.GoogleMapsCoordinates;

public interface GoogleMapsCoordinatesRepository extends CrudRepository<GoogleMapsCoordinates, Integer> {

}
