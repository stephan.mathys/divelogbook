package ch.hotmilk.dlb.server.repository;

import org.springframework.data.repository.CrudRepository;

import ch.hotmilk.dlb.server.model.Keyword;

public interface KeywordRepo extends CrudRepository<Keyword, Integer> {

}
