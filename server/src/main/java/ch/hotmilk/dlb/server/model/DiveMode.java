package ch.hotmilk.dlb.server.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum DiveMode {

	FREEDIVE, GAUGE, @JsonProperty("opencircuit")
	OPENCIRCUIT, CLOSEDCIRCUIT;
}
