package ch.hotmilk.dlb.server.model.adapter;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

public class DiveMaxDepthAdapter extends StdDeserializer<Double> {

	private static final long serialVersionUID = -7922099506988408661L;

	protected DiveMaxDepthAdapter(Class<?> vc) {
		super(vc);
	}

	public DiveMaxDepthAdapter() {
		this(null);
	}

	@Override
	public Double deserialize(JsonParser p, DeserializationContext ctxt) throws IOException, JsonProcessingException {
		System.out.println();
		JsonDeserializer<Object> mapDeserializer = findDeserializer(ctxt,
				ctxt.getTypeFactory().constructMapType(HashMap.class, String.class, Object.class), null);
		@SuppressWarnings("unchecked")
		Map<String, Object> rawValue = (Map<String, Object>) mapDeserializer.deserialize(p, ctxt);
		if (rawValue == null) {
			return null;
		}
		Properties mappedValue = new Properties();
		rawValue.forEach((key, value) -> mappedValue.put(key, value));
		return Double.valueOf((String) mappedValue.get("max"));

	}

}
