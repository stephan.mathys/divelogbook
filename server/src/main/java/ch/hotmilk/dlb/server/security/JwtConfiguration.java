package ch.hotmilk.dlb.server.security;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import lombok.Getter;
import lombok.Setter;

@Component
@ConfigurationProperties(prefix = "ch.hotmilk.jwt.aws")
public class JwtConfiguration {
	@Setter
	@Getter
	private String userPoolId;
	@Setter
	@Getter
	private String identityPoolId;
	@Setter
	@Getter
	private String jwkUrl;
	@Setter
	@Getter
	private String region;
	@Setter
	@Getter
	private String userNameField;
	@Setter
	@Getter
	private String userSubField;
	@Setter
	@Getter
	private String scopeField;
	@Setter
	@Getter
	private int connectionTimeout;
	@Setter
	@Getter
	private int readTimeout;
	@Setter
	@Getter
	private String httpHeader;
	@Setter
	@Getter
	private String cognitoIdentityPoolUrl;

//	public String getJwkUrl() {
//
//		return this.jwkUrl != null && !this.jwkUrl.isEmpty() ? this.jwkUrl
//				: String.format("https://cognito-idp.%s.amazonaws.com/%s/.well-known/jwks.json", this.region,
//						this.userPoolId);
//	}

//	public String getCognitoIdentityPoolUrl() {
//		return String.format("https://cognito-idp.%s.amazonaws.com/%s", this.region, this.userPoolId);
//	}
}
