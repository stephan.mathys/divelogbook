package ch.hotmilk.dlb.server.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import ch.hotmilk.dlb.server.json.serializer.DiveTimeAndDateSerializer;
import ch.hotmilk.dlb.server.model.Dive;
import ch.hotmilk.dlb.server.model.Userpool;
import ch.hotmilk.dlb.server.service.DiveService;
import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
//@CrossOrigin(origins = "http://localhost:8080", maxAge = 3600)
public class DiveController {

	@Autowired
	private DiveService diveService;

	@GetMapping("getDives")
	public String getAllDives() {

		List<Dive> allDives = diveService.getAllDives();
		ObjectMapper objectMapper = new ObjectMapper();
		String jsonDives;
		try {
			jsonDives = objectMapper.writeValueAsString(allDives);
		} catch (JsonProcessingException e) {
			jsonDives = "could not converte dives to json";
			e.printStackTrace();

		}
		return jsonDives;
	}

	@PutMapping("getDivesDateTime")
	public String updateDives(@RequestBody String strDives) throws JsonMappingException, JsonProcessingException {
		log.debug(strDives.toString());
		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		objectMapper.registerModule(new JavaTimeModule());
		objectMapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);

		TypeReference<List<Dive>> typeRef = new TypeReference<List<Dive>>() {
		};

		List<Dive> dives = objectMapper.readValue(strDives, typeRef);

		diveService.updateCoordinates(dives);
		return "ok";
	}

	@GetMapping("getDivesDateTime")
	public String getAllDivesWithDateAndTime(Authentication authentication) {
//		List<Dive> dives = diveService.getAllDives();
		List<Dive> dives = diveService.getAllDivesFromUser((Userpool) authentication.getPrincipal());
		ObjectMapper objectMapper = new ObjectMapper();
		SimpleModule module = new SimpleModule();
		module.addSerializer(Dive.class, new DiveTimeAndDateSerializer());
		objectMapper.registerModule(module);

		String jsonDives;
		try {
			jsonDives = objectMapper.writeValueAsString(dives);
		} catch (JsonProcessingException e) {
			jsonDives = "could not converte dives to json";
			e.printStackTrace();

		}
		log.debug("get Dives from user: " + jsonDives);
		return jsonDives;
	}

//	@CrossOrigin
	@GetMapping("dive")
	public String getDive(@RequestParam(name = "diveId") String diveId, Authentication authentication) {
		Integer diveIdInt = Integer.valueOf(diveId);
		log.debug("get Dive with id: " + diveIdInt);
		Dive d = diveService.getDive(diveIdInt, (Userpool) authentication.getPrincipal());
		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.registerModule(new JavaTimeModule());
		objectMapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
		String jsonDives;
		try {
			jsonDives = objectMapper.writeValueAsString(d);
		} catch (JsonProcessingException e) {
			jsonDives = "could not converte dives to json";
			e.printStackTrace();

		}
		log.debug("get Dive from user: " + jsonDives);
		return jsonDives;
	}
}
