package ch.hotmilk.dlb.server.repository;

import org.springframework.data.repository.CrudRepository;

import ch.hotmilk.dlb.server.model.Gasmix;

public interface GasmixRepository extends CrudRepository<Gasmix, Integer> {

}
