package ch.hotmilk.dlb.server.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@ToString
@Entity
@NoArgsConstructor
public class DiveComputer {

	public DiveComputer(String vendor, String product, String model) {
		this.vendor = vendor;
		this.product = product;
		this.model = model;
	}

	@Getter
	@Setter
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Getter
	@Setter
	private String vendor;

	@Getter
	@Setter
	private String product;

	@Getter
	@Setter
	private String model;

}
