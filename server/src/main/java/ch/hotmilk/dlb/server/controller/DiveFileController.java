package ch.hotmilk.dlb.server.controller;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import ch.hotmilk.dlb.server.service.DiveFileUploadService;
import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
public class DiveFileController {

	@Autowired
	DiveFileUploadService uploadService;

	@PostMapping("/uploadFile")
	public HttpStatus uploadFile(@RequestParam("file") MultipartFile file) {
		try {
			uploadService.saveDiveFile(file.getInputStream());
		} catch (IOException e) {
			log.debug("Could not upload file", e);
			return HttpStatus.INTERNAL_SERVER_ERROR;
		}

		return HttpStatus.OK;
	}
}
