package ch.hotmilk.dlb.server.repository;

import org.springframework.data.repository.CrudRepository;

import ch.hotmilk.dlb.server.model.Deco;

public interface DecoRepository extends CrudRepository<Deco, Integer> {

}
