
CREATE TABLE userpool (
  id INT AUTO_INCREMENT PRIMARY KEY,
  sub VARCHAR(250)
);

CREATE TABLE deco(
  id INT AUTO_INCREMENT  PRIMARY KEY,
  depth DOUBLE,
  type VARCHAR(250),
  duration BIGINT
);

CREATE TABLE dive_computer(
  id INT AUTO_INCREMENT  PRIMARY KEY,
  vendor  VARCHAR(250),
  product  VARCHAR(250),
  model  VARCHAR(250) 
);

CREATE TABLE google_maps_coordinates(
	id INT AUTO_INCREMENT  PRIMARY KEY,
	latitude DOUBLE,
	longitude DOUBLE
);
CREATE TABLE keyword(
	id INT AUTO_INCREMENT,
	name VARCHAR(250),
 	PRIMARY KEY (id)	
);

CREATE TABLE dive (
  id INT AUTO_INCREMENT,
  dive_computer_id INT,
  google_maps_coordinates_id INT,
  userpool_sub VARCHAR(250),
  internal_number INT,
  fingerprint VARCHAR(250),
  duration INT,
  dive_date DATE,
  dive_duration BIGINT,
  dive_mode VARCHAR(250),
  dive_start_time TIME,
  max_depth DOUBLE,
  PRIMARY KEY (id),
  CONSTRAINT dive_ibfk_1 FOREIGN KEY (dive_computer_id) REFERENCES dive_computer (id),
  CONSTRAINT dive_ibfk_2 FOREIGN KEY (google_maps_coordinates_id) REFERENCES google_maps_coordinates (id)
);

CREATE TABLE dive_keyword(
	dive_id INT NOT NULL,
	keyword_id INT NOT NULL,
  	PRIMARY KEY (dive_id, keyword_id),	
    CONSTRAINT dive_keyword_ibfk_1 FOREIGN KEY (dive_id) REFERENCES dive (id),
    CONSTRAINT dive_keyword_ibfk_2 FOREIGN KEY (keyword_id) REFERENCES keyword (id)
);


CREATE TABLE dive_sample(
  id INT AUTO_INCREMENT,  
  deco_id INT,
  dive_id INT,
  time BIGINT,
  depth DOUBLE,
  wather_temperature DOUBLE,
  gas_change INT,
  PRIMARY KEY (id),
  CONSTRAINT dive_sample_ibfk_1 FOREIGN KEY (dive_id) REFERENCES dive (id),
  CONSTRAINT dive_sample_ibfk_2 FOREIGN KEY (deco_id) REFERENCES deco (id)
);

CREATE TABLE gasmix(
  id INT AUTO_INCREMENT,  
  dive_id INT,
  number INT,
  oxygen DOUBLE,
  nitrogen DOUBLE,
  helium DOUBLE,
  PRIMARY KEY (id),
  CONSTRAINT gasmix_ibfk_1 FOREIGN KEY (dive_id) REFERENCES dive (id)
);
