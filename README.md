# DiveLogbook

## Server

### Environment
Setup for the Development Environment

First install a jdk. Preferred way with sdkMan:
https://sdkman.io/
We use version "17.0.1-open"

Install latest eclipse. Here we use the project Lombok:
https://projectlombok.org/

You have to copy/link an *application-[dev|test].yml* to ~/git/divelogbook/server/src/main/ressources. Here we have a script if you have access to the repo divelogbook.private in the folder scripts. If you run the script the first param is your gitlab username.
If you don't have access, then you have to write the *application-[dev|test].yml* by yourself. You can find a template for that in the application.yml file.

You can choose if you want to run the application in development or test mode. 

**Development mode:**
You have to start the application with the project property profile=dev:
Run configuration in eclipse: Use '-Dspring.profiles.active=dev' as VM argument to start the application in development mode
gradle command: ``./gradlew bootRun -Pprofile=dev``

**Test mode:**
*Setting up the database*
For setting up the database you have to build an image from the dockerfile in the folder './docker/mysql':
`docker build -t divelog-mysql-test .` To start the image `docker run divelog-mysql-test`
*Run the application*
You have to start the application with the project property profile=test:
Run configuration in eclipse: Use '-Dspring.profiles.active=test' as VM argument to start the application in development mode
gradle command: ``./gradlew bootRun -Pprofile=test``

If you switch between modes and want to start it from eclipse make sure you run the gradle tasks *cleanEclipse* and *eclipse* with the profile property. After you run you have to refresh the package Explorer.
 
## Client
TODO

