module.exports = {
  module: {
    loaders: [
      {
        test: /\.vue$/,
        loader: "vue",
      },
      {
        test: /\.s[a|c]ss$/,
        loader: "style!css!sass",
      },
    ],
  },
  vue: {
    loaders: {
      scss: "style!css!sass",
    },
  },
};

// UNTEN IST VON VUE.ORG
// module.exports = {
//   module: {
//     rules: [
//       // ... other rules omitted

//       // this will apply to both plain `.scss` files
//       // AND `<style lang="scss">` blocks in `.vue` files
//       {
//         test: /\.scss$/,
//         use: [
//           'vue-style-loader',
//           'css-loader',
//           'sass-loader'
//         ]
//       }
//     ]
//   },
//   // plugin omitted
// }
