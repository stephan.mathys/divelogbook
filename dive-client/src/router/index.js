import Vue from "vue";
import VueRouter from "vue-router";
import home from "@/views/Home";
import about from "@/views/About";
import frontDoor from "@/components/FrontDoor";

import metaDetailData from "@/components/DiveMetadata";
import diveDetailMap from "@/components/diveDetailMap";
import diveAdditionalData from "@/components/diveDetailDataMain";
import diveLineChart from "@/components/DiveLineChart";

import editDive from "@/views/EditDive";
import dive from "@/views/Dive";
import store from "@/store";
import axios from "axios";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Home",
    component: home,
    meta: { requiresAuth: false },
  },
  {
    path: "/login",
    name: "Logout",
    component: frontDoor,
    meta: { requiresAuth: false },
  },
  {
    path: "/login",
    name: "Login",
    component: frontDoor,
    meta: { requiresAuth: false },
  },
  {
    path: "/about",
    name: "About",
    component: about,
    meta: { requiresAuth: false },
  },
  {
    path: "/edit-dives",
    name: "EditDives",
    component: editDive,
    meta: { requiresAuth: true },
    props: { editMode: true },
  },
  {
    path: "/diveDetail",
    name: "diveDetail",
    component: dive,
    meta: { requiresAuth: true },
    props: { editMode: false },
    children: [
      {
        path: ":diveId",
        name: "diveId",
        props: true,
        components: {
          default: metaDetailData,
          map: diveDetailMap,
          additionalData: diveAdditionalData,
          lineChart: diveLineChart,
        },
      },
    ],
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

router.beforeEach(async (to, from, next) => {
  if (to.meta.requiresAuth && !store.getters.isUserSignedIn) {
    // this route requires auth, check if logged in
    // if not, redirect to login page.
    return next({
      path: "/login",
      query: { redirect: to.fullPath },
    });
  } else if (to.name == "Logout") {
    store.commit("signOut");
  } else if (to.name == "diveId") {
    await axios
      .get("dive", {
        params: {
          diveId: to.params.diveId,
        },
      })
      .then((response) => {
        store.commit("updateCurrentDive", response.data);
      });
  } else if (from.name == "EditDives") {
    while (store.getters.getCheckedDives.length > 0) {
      store.commit("removeCheckedDive", store.getters.getCheckedDives[0]);
    }
  }
  next();
});

export default router;
