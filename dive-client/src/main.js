import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store/index.js";
import * as VueGoogleMaps from "vue2-google-maps";
import axios from "axios";
import VueAxios from "vue-axios";
import apolloProvider from "@/appsync";
import Amplify, * as AmplifyModules from "aws-amplify";
import { AmplifyPlugin } from "aws-amplify-vue";
import awsmobile from "@/aws-exports";
// custom styles
import "./assets/css/styles.sass";
import "./assets/css/styles800breakpoint.sass";
import "./assets/css/styles1200breakpoint.sass";
import "./assets/css/themeSwitchStyles.sass";
import "./assets/css/bubbleCreationStyles.sass";

Amplify.configure(awsmobile);

Vue.config.productionTip = false;

Vue.use(AmplifyPlugin, AmplifyModules);

Vue.use(VueGoogleMaps, {
  load: {
    key: process.env.VUE_APP_GOOGLE_MAPS_KEY,
    libraries: "places", // necessary for places input
  },
});

Vue.use(VueAxios, axios);

axios.interceptors.request.use((config) => {
  config.headers.Authorization = "Bearer " + store.state.jwtToken;
  config.url = process.env.VUE_APP_SERVER_BASE_URL + config.url;
  console.log(config);
  return config;
});

axios.interceptors.response.use(
  function (successRes) {
    console.log(successRes);
    return successRes;
  },
  function (error) {
    console.log(error);
    //return Promise.reject(error);
  }
);

Vue.use(require("vue-moment"));

new Vue({
  router,
  store,
  apolloProvider,
  render: (h) => h(App),
}).$mount("#app");
