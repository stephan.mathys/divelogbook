import Vue from "vue";
import Vuex from "vuex";
Vue.use(Vuex);
import { Auth } from "aws-amplify";

export default new Vuex.Store({
  state: {
    currentDive: null,
    navigationData: null,
    userSignedIn: false,
    checkedDives: new Array(),
    jwtToken: null,
    filterPlaces: process.env.VUE_APP_FILTER_PLACES_ALL,
    editMarker: {},
  },
  mutations: {
    async signOut(state) {
      try {
        console.log("try to signOut");
        await Auth.signOut();
        state.jwtToken = null;
        state.userSignedIn = false;
        state.dives = null;
        state.navigationData = null;
        state.currentDive = null;
        state.checkedDives = new Array();
        state.filterPlaces = process.env.VUE_APP_FILTER_PLACES_ALL;
        state.editMarker = {};
        console.log("signOut successfully");
      } catch (error) {
        console.log("error signing out: ", error);
      }
    },
    addCheckedDive(state, diveId) {
      const index = state.checkedDives.indexOf(diveId.toString());
      if (index == -1) {
        state.checkedDives.push(diveId.toString());
        const dive = state.navigationData.find((e) => e.id == diveId);
        dive.oldCoordinates = Vue.util.extend({}, dive.googleMapsCoordinates);
        dive.changed = true;
        if (state.editMarker.position != null) {
          dive.googleMapsCoordinates.lat = state.editMarker.position.lat;
          dive.googleMapsCoordinates.lng = state.editMarker.position.lng;
        }
      }
    },
    removeCheckedDive(state, diveId) {
      const index = state.checkedDives.indexOf(diveId.toString());
      if (index > -1) {
        state.checkedDives.splice(index, 1);
      } else {
        return;
      }
      const dive = state.navigationData.find((e) => e.id == diveId);
      dive.googleMapsCoordinates.lat = dive.oldCoordinates.lat;
      dive.googleMapsCoordinates.lng = dive.oldCoordinates.lng;
      dive.oldCoordinates = null;
    },
    updateEditMarkerPosition(state, position) {
      state.editMarker = { position: position };
      state.checkedDives.forEach((diveId) => {
        const dive = state.navigationData.find((e) => e.id == diveId);
        dive.googleMapsCoordinates.lat = state.editMarker.position.lat;
        dive.googleMapsCoordinates.lng = state.editMarker.position.lng;
      });
    },
    updateFilterPlaces(state, value) {
      state.filterPlaces = value;
    },
    signIn(state, signIn) {
      state.username = signIn.username;
      state.jwtToken = signIn.jwt;
      state.userSignedIn = true;
      console.log("SignIn successfully");
    },
    updateJwtToken(state, token) {
      state.jwtToken = token;
      console.log("token updated: " + token);
    },
    updateCurrentUser(state, user) {
      console.log("user to store:");
      console.log(user);
    },
    updateCurrentDive(state, dive) {
      state.currentDive = dive;
      console.log("Dive commited: " + dive.id);
    },
    navigationData(state, navData) {
      state.navigationData = navData;
      console.log("Navigation commited: " + navData.length);
    },
  },
  actions: {},
  modules: {},
  getters: {
    getJwt: (state) => {
      return state.jwtToken;
    },
    //Attention!
    //Here we store the last gasmix description
    //because at the moment we only have one gasmix per dive.
    //It is ok. But if we have more than one, ->
    //Houston, we have a problem!
    getGasmix: (state) => {
      const gasmixes = state.currentDive.gasmixes;
      if (gasmixes.length == 1) {
        return gasmixes[0];
      } else if (gasmixes.length > 1) {
        console.log(
          "more than one gasmix für dive with id " + state.currentDive.id
        );
      }
      return null;
    },
    isUserSignedIn: (state) => {
      console.log("isUserSignedIn: " + state.userSignedIn);
      return state.userSignedIn;
    },
    isIdChecked: (state) => (diveId) => {
      const isIdChecked =
        state.checkedDives.find((id) => id == diveId) != undefined;
      return isIdChecked;
    },
    getCheckedDives: (state) => {
      return state.checkedDives;
    },
  },
});
