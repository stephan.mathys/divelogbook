export default {
  dives: [
    {
      id: "1",
      date: "26.02.2020 - 08:00",
      isLocationSet: true,
      divetime: 30,
      maxdepth: 15,
      googleMapCoordinates: {
        id: 4,
        lat: 80.3214,
        lng: 20.3424,
      },
      gasmix: {
        gas: "Nitrox",
        o2: 31,
        n2: 69,
        he: 0,
      },
      computer: {
        model: "Zoop",
        vendor: "Suntoo",
        product: "product",
      },
      samples: [
        {
          id: "1",
          depth: "1.2",
          time: "0",
        },
        {
          id: "2",
          depth: "1.4",
          time: "2",
        },
        {
          id: "3",
          depth: "2.5",
          time: "4",
        },
        {
          id: "4",
          depth: "5",
          time: "6",
        },
        {
          id: "5",
          depth: "10",
          time: "8",
        },
        {
          id: "6",
          depth: "15",
          time: "10",
        },
        {
          id: "7",
          depth: "10",
          time: "12",
        },
        {
          id: "8",
          depth: "6",
          time: "14",
        },
        {
          id: "9",
          depth: "3",
          time: "16",
        },
        {
          id: "10",
          depth: ".8",
          time: "18",
        },
      ],
    },
    {
      id: "2",
      date: "26.02.2020 - 10:00",
      isLocationSet: true,
      divetime: 45,
      maxdepth: 30,
      googleMapCoordinates: {
        id: 1,
        lat: 40.0,
        lng: 24.0,
      },
      gasmix: {
        gas: "Nitrox",
        o2: 30,
        n2: 70,
        he: 0,
      },
      computer: {
        model: "Zoop",
        vendor: "Suntoo",
        product: "product",
      },
      samples: [
        {
          id: "1",
          depth: "1.2",
          time: "0",
        },
        {
          id: "2",
          depth: "5",
          time: "2",
        },
        {
          id: "3",
          depth: "12",
          time: "4",
        },
        {
          id: "4",
          depth: "18.5",
          time: "6",
        },
        {
          id: "5",
          depth: "22",
          time: "8",
        },
        {
          id: "6",
          depth: "12",
          time: "10",
        },
        {
          id: "7",
          depth: "7",
          time: "12",
        },
        {
          id: "8",
          depth: "3.4",
          time: "14",
        },
        {
          id: "9",
          depth: "3",
          time: "16",
        },
        {
          id: "10",
          depth: "0.5",
          time: "18",
        },
      ],
    },
    {
      id: "3",
      date: "26.02.2020 - 14:00",
      isLocationSet: true,
      divetime: 50,
      maxdepth: 18,
      googleMapCoordinates: {
        id: 1,
        lat: 47.324,
        lng: 8.3424,
      },
      gasmix: {
        gas: "Nitrox",
        o2: 29,
        n2: 71,
        he: 0,
      },
      computer: {
        model: "Zoop",
        vendor: "Suntoo",
        product: "product",
      },
      samples: [
        {
          id: "1",
          depth: "1.2",
          time: "0",
        },
        {
          id: "2",
          depth: "8",
          time: "2",
        },
        {
          id: "3",
          depth: "15",
          time: "4",
        },
        {
          id: "4",
          depth: "22",
          time: "6",
        },
        {
          id: "5",
          depth: "28",
          time: "8",
        },
        {
          id: "6",
          depth: "31",
          time: "10",
        },
        {
          id: "7",
          depth: "28",
          time: "12",
        },
        {
          id: "8",
          depth: "20",
          time: "14",
        },
        {
          id: "9",
          depth: "16",
          time: "16",
        },
        {
          id: "10",
          depth: "15",
          time: "18",
        },
      ],
    },
    {
      id: "4",
      date: "27.02.2020 - 08:00",
      isLocationSet: false,
      divetime: 20,
      maxdepth: 15,
      googleMapCoordinates: {
        id: 0,
        lat: 0,
        lng: 0,
      },
      gasmix: {
        gas: "Air",
        o2: 21,
        n2: 79,
        he: 0,
      },
      computer: {
        model: "Zoop",
        vendor: "Suntoo",
        product: "product",
      },
      samples: [
        {
          id: "1",
          depth: "0.7",
          time: "0",
        },
        {
          id: "2",
          depth: "3.6",
          time: "2",
        },
        {
          id: "3",
          depth: "8.1",
          time: "4",
        },
        {
          id: "4",
          depth: "13",
          time: "6",
        },
        {
          id: "5",
          depth: "14",
          time: "8",
        },
        {
          id: "6",
          depth: "10",
          time: "10",
        },
        {
          id: "7",
          depth: "13",
          time: "12",
        },
        {
          id: "8",
          depth: "12",
          time: "14",
        },
        {
          id: "9",
          depth: "8",
          time: "16",
        },
        {
          id: "10",
          depth: "3",
          time: "18",
        },
      ],
    },
    {
      id: "5",
      date: "28.02.2020 - 08:00",
      isLocationSet: true,
      divetime: 40,
      maxdepth: 30,
      googleMapCoordinates: {
        id: 1,
        lat: 47.324,
        lng: 8.3424,
      },
      gasmix: {
        gas: "Air",
        o2: 21,
        n2: 79,
        he: 0,
      },
      computer: {
        model: "Zoop",
        vendor: "Suntoo",
        product: "product",
      },
      samples: [
        {
          id: "1",
          depth: "0.7",
          time: "0",
        },
        {
          id: "2",
          depth: "3.6",
          time: "2",
        },
        {
          id: "3",
          depth: "8.1",
          time: "4",
        },
        {
          id: "4",
          depth: "13",
          time: "6",
        },
        {
          id: "5",
          depth: "14",
          time: "8",
        },
        {
          id: "6",
          depth: "10",
          time: "10",
        },
        {
          id: "7",
          depth: "13",
          time: "12",
        },
        {
          id: "8",
          depth: "12",
          time: "14",
        },
        {
          id: "9",
          depth: "8",
          time: "16",
        },
        {
          id: "10",
          depth: "3",
          time: "18",
        },
      ],
    },
    {
      id: "6",
      date: "28.02.2020 - 08:00",
      isLocationSet: true,
      divetime: 40,
      maxdepth: 30,
      googleMapCoordinates: {
        id: 1,
        lat: 34.324,
        lng: 20.3424,
      },
      gasmix: {
        gas: "Air",
        o2: 21,
        n2: 79,
        he: 0,
      },
      computer: {
        model: "Zoop",
        vendor: "Suntoo",
        product: "product",
      },
      samples: [
        {
          id: "1",
          depth: "0.7",
          time: "0",
        },
        {
          id: "2",
          depth: "3.6",
          time: "2",
        },
        {
          id: "3",
          depth: "8.1",
          time: "4",
        },
        {
          id: "4",
          depth: "13",
          time: "6",
        },
        {
          id: "5",
          depth: "14",
          time: "8",
        },
        {
          id: "6",
          depth: "10",
          time: "10",
        },
        {
          id: "7",
          depth: "13",
          time: "12",
        },
        {
          id: "8",
          depth: "12",
          time: "14",
        },
        {
          id: "9",
          depth: "8",
          time: "16",
        },
        {
          id: "10",
          depth: "3",
          time: "18",
        },
      ],
    },
  ],
};
